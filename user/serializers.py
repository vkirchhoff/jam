from rest_framework import serializers
from django.contrib.auth import get_user_model
from main.serializers import InstrumentSerializer
from main.models import Instrument

User = get_user_model()


class UserListSerializer(serializers.ModelSerializer):
    instruments = InstrumentSerializer(many=True)

    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'email', 'first_name', 'last_name', 'instruments', ]


class UserRegistrationSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    def create(self, validated_data):
        password = validated_data.pop('password')

        user = User.objects.create(
            **validated_data
        )

        user.set_password(password)
        user.save()
        return user

    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'email', 'first_name', 'last_name', ]


class UserUpdateSerializer(serializers.ModelSerializer):
    instruments = serializers.PrimaryKeyRelatedField(
        many=True, required=False, allow_null=True,
        queryset=Instrument.objects.all()
    )

    class Meta:
        model = User
        fields = ['id', 'email', 'first_name', 'last_name', 'instruments', ]
