from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    instruments = models.ManyToManyField('main.Instrument', blank=True)
