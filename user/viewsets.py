from rest_framework import viewsets, mixins, status
from rest_framework.response import Response
from rest_framework.decorators import action
from django.contrib.auth import get_user_model
from .serializers import UserListSerializer, UserRegistrationSerializer, UserUpdateSerializer


User = get_user_model()


class UserViewSet(mixins.ListModelMixin,
                  mixins.RetrieveModelMixin,
                  mixins.UpdateModelMixin,
                  viewsets.GenericViewSet):
    def get_queryset(self):
        return User.objects.prefetch_related('instruments').all()

    def get_serializer_class(self):
        if self.action in ('list', 'retrieve', 'delete'):
            return UserListSerializer
        elif self.action in ('register', ):
            return UserRegistrationSerializer
        return UserUpdateSerializer

    @action(detail=False, methods=['post', ], url_path='register')
    def register(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
