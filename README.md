## **Introduction**

This is an MVP app to provide a music jamming functionality.

Source code: https://gitlab.com/vkirchhoff/jam

Demo stand is located at https://jam.vkirchhoff.com/ (frontend stub).

API docs: https://jam.vkirchhoff.com/api/docs/

Admin Panel: https://jam.vkirchhoff.com/admin/

```
Credentials for the Admin Panel
login: admin
password: 1234qwer
```

### **Demo scenario**

Go to API docs https://jam.vkirchhoff.com/api/docs/. 
There you can find several API methods. 
Lets start with the one that registers new users.

1) Use ``POST /user/register/`` to create some users (you will find tips how to fill in the data there).

2) Then let's create some instruments: ``POST /instrument/``
3) Update users to add instruments: ``PATCH /user/{id}/``
4) Now create jam: ``POST /jam/``
5) Create several jam-entries: ``POST /jam-entry/`` (only .wav files supported for sound_files field).
6) Create test song: ``POST /song/``.
    ```
    SongEntry params description:
    
    a) SongEntries with the same stream value will be concatenated according to their order params 
    (offsets between concatenated parts might be added later)
    
    b) Concatenated streams will be overlayed
    
   example:
   
   {
        "name": "string",
        "entries": [
            {
                "jam_entry": 3,
                "stream": 0,
                "order": 0,
                "lasts": 0
            },
            {
                "jam_entry": 1,
                "stream": 0,
                "order": 1,
                "lasts": 0
            },
            {
                "jam_entry": 4,
                "stream": 0,
                "order": 1,
                "lasts": 0
            }
        ],
        "description": ""
    }
   ```
7) Download the soundtrack via a direct link received in the response or via the admin panel.


### **Local installation**

In case you still wish to install on your local machine.

1) Install python and devlibs ``apt-get install python3 python3-dev python3-pip libasound2-dev``
2) Install virtualenv globally ``pip3 install virtualenv``
3) Clone project
4) Move into project's directory ``cd jam``
5) Create virtualenv ``virtualenv venv``
6) Activate venv ``source venv/bin/activate``
7) ``pip3 install -r requirements.txt``
8) Copy config ``cp jam/.env.sample jam/.env`` and amend values if needed
9) Get SECRET_KEY ``export SECRET_KEY='9)cw)e$w35er0ug%z%ns14d*7&opro20rzl@@w54rl0va-p_2f'``
10) Get a postgres https://www.postgresql.org/download/ instance or a Docker container running it
11) collect static ``python3 manage.py collectstatic``
12) run dev server ``python3 manage.py runserver``