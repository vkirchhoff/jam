import os
from fabric.api import env, hide, abort, run, cd, shell_env
from fabric.contrib.project import rsync_project

env.user = 'root'
env.abort_on_prompts = True
ENV_FILE = '/etc/profile.d/variables.sh'
PATH = '/jam'
VARIABLES = ('SECRET_KEY', 'CI_BUILD_TOKEN', )


def _rsync():
    exclusions = ('.git*', '.env', '*.sock*', '*.lock', '*.pyc', '*cache*', '*.log', 'log/',
                  'id_rsa*', 'static/', 'data/', 'frontend/', 'venv/')
    rsync_project(PATH, './', exclude=exclusions, delete=True)


def _docker_compose(command):
    with cd(PATH):
        with shell_env(
            CI_BUILD_REF_NAME=os.getenv(
                'CI_BUILD_REF_NAME', 'master'
            ),
            SECRET_KEY=os.getenv(
                'SECRET_KEY'
            )
        ):
            # hiding progress bar, https://git.io/vXH8a
            run('set -o pipefail; docker-compose %s | tee' % command)


def _run_manage_py(command):
    run('docker exec -it jam-web python3 manage.py %s' % command)


def deploy():
    run('docker login -u %s -p %s %s' % (os.getenv('REGISTRY_USER',
                                                   'gitlab-ci-token'),
                                         os.getenv('CI_BUILD_TOKEN'),
                                         os.getenv('CI_REGISTRY',
                                                   'registry.gitlab.com')))
    _rsync()
    _docker_compose('pull')
    _docker_compose('up -d')
    _run_manage_py('collectstatic --noinput')
    _run_manage_py('migrate')
