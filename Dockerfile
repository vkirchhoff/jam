FROM alpine:3.8

ARG SECRET_KEY
ARG CI_COMMIT_BRANCH

RUN export SECRET_KEY=$SECRET_KEY

RUN mkdir -p /app
WORKDIR /app

RUN apk update
RUN apk add --no-cache --virtual .build-deps g++ python3-dev libffi-dev libressl-dev alsa-lib-dev && \
    apk add --no-cache --update python3 && \
    pip3 install --upgrade pip setuptools
RUN apk add --no-cache --virtual .build-deps gcc musl-dev linux-headers pkgconf \
    autoconf automake libtool make postgresql-dev postgresql-client  && \
    apk add postgresql-libs postgresql-client && \
    # preventing unsuccessful uWSGI compilation in Docker, https://git.io/v1ve3
    (while true; do pip --no-cache-dir install uwsgi==2.0.19 && break; done)

COPY requirements.txt /app
RUN pip install --no-cache-dir -r requirements.txt

COPY . /app

RUN cp jam/.env.$CI_COMMIT_BRANCH jam/.env
RUN apk del .build-deps
