import io
from django.db import models
from django.core.files import File
from django.contrib.auth import get_user_model
from django.utils import timezone
from . import errors
from django.core.validators import FileExtensionValidator
from collections import defaultdict
from functools import reduce
from .helpers import get_ext
from pydub import AudioSegment
from pydub.playback import play

User = get_user_model()

ALLOWED_SOUND_EXTENSIONS = ('wav', 'mp3', )


class Instrument(models.Model):
    name = models.CharField(max_length=64, blank=False, null=False)
    description = models.TextField(max_length=1024, blank=True, null=False)

    class Meta:
        verbose_name = 'Instrument'
        verbose_name_plural = 'Instruments'

    def __str__(self):
        return self.name


class Jam(models.Model):
    OPENED = 'opened'
    STARTED = 'started'
    FINISHED = 'finished'

    STATUS_CHOICES = (
        (OPENED, 'Opened'),
        (STARTED, 'Started'),
        (FINISHED, 'Finished'),
    )

    status = models.CharField(max_length=16, choices=STATUS_CHOICES, default=OPENED, blank=False, null=False)
    date_started = models.DateTimeField(blank=False, null=False, default=timezone.now)
    date_finished = models.DateTimeField(blank=True, null=True)
    required_instruments = models.ManyToManyField(Instrument, blank=True, related_name='requiring_jams')
    used_instruments = models.ManyToManyField(Instrument, blank=True, through='JamEntry', related_name='using_jams')
    users = models.ManyToManyField(User, blank=True, through='JamEntry')

    class Meta:
        verbose_name = 'Jam'
        verbose_name_plural = 'Jams'

    def _validate_required_instruments(self):
        used_instrument_ids = self.used_instruments.all().values_list('id', flat=True)
        for required_instrument_id in self.required_instruments.all().values_list('id', flat=True):
            if required_instrument_id not in used_instrument_ids:
                raise errors.NotEnoughInstrumentsError

    def start(self):
        self._validate_required_instruments()
        self.status = self.STARTED
        self.save()

    def __str__(self):
        return f'Jam #{self.pk}'


class JamEntry(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=False, null=False)
    jam = models.ForeignKey(Jam, on_delete=models.CASCADE, blank=False, null=False, related_name='entries')
    instrument = models.ForeignKey(Instrument, on_delete=models.CASCADE, blank=False, null=False)
    sound_file = models.FileField(
        blank=True, null=False, validators=[FileExtensionValidator(ALLOWED_SOUND_EXTENSIONS)]
    )

    class Meta:
        verbose_name = 'Jam Entry'
        verbose_name_plural = 'Jam Entries'

    def _validate_extension(self):
        if self.sound_file:
            ext = self.sound_file.file.name.split('.')[-1]
            if ext not in ALLOWED_SOUND_EXTENSIONS:
                raise ValueError(f'{ext} is not in ALLOW_SOUND_EXTENSION list')

    def _validate_instruments(self):
        if self.instrument not in self.user.instruments.all():
            raise errors.NonSupportedInstrumentError()

    def save(self, **kwargs):
        self._validate_extension()
        self._validate_instruments()
        return super(JamEntry, self).save(**kwargs)

    def __str__(self):
        return f'{self.user} ({self.jam_id} <- {self.instrument})'


class Song(models.Model):
    name = models.CharField(max_length=64, blank=False, null=False)
    description = models.TextField(max_length=1024, blank=True, null=False)
    sound_file = models.FileField(
        blank=True, null=False, validators=[FileExtensionValidator(ALLOWED_SOUND_EXTENSIONS)]
    )

    class Meta:
        verbose_name = 'Song'
        verbose_name_plural = 'Songs'

    def _group_by_streams(self):
        entries = self.entries.all().select_related('jam_entry').order_by('stream', 'order')
        streams = defaultdict(list)
        for entry in entries:
            streams[entry.stream].append(entry)
        return streams

    @staticmethod
    def _concatenate_stream(stream_entries):
        return sum(
            AudioSegment.from_file(
                entry.jam_entry.sound_file.path,
                get_ext(entry.jam_entry.sound_file.name)
            )
            for entry in stream_entries
        )

    @staticmethod
    def _overlay_streams(streams):
        streams.sort(key=lambda stream: -len(stream))
        return reduce(AudioSegment.overlay, streams)

    def _save_song(self, song):
        song_buffer = io.BytesIO()
        song.export(song_buffer, format='wav')

        self.sound_file = File(song_buffer, name=f'{self.name}.wav')
        self.save()

    def create(self):
        streams = self._group_by_streams()
        concatenated_streams = [self._concatenate_stream(streams[stream_key]) for stream_key in streams]
        song = self._overlay_streams(concatenated_streams)
        self._save_song(song)

    # to be used locally to play the result
    def play(self):
        if self.sound_file:
            play(
                AudioSegment.from_file(
                    self.sound_file.path,
                    get_ext(self.sound_file.name)
                )
            )
        return

    def __str__(self):
        return self.name


class SongEntry(models.Model):
    song = models.ForeignKey(Song, on_delete=models.CASCADE, blank=False, null=False, related_name='entries')
    jam_entry = models.ForeignKey(JamEntry, on_delete=models.PROTECT, blank=False, null=False)
    stream = models.SmallIntegerField(default=0)
    order = models.SmallIntegerField(default=0)
    lasts = models.IntegerField(default=0)

    class Meta:
        verbose_name = 'Song Entry'
        verbose_name_plural = 'Song Entries'

    def _validate_jam_is_the_same(self):
        song_entry = SongEntry.objects.select_related('jam_entry').filter(
            song=self.song
        ).exclude(pk=self.pk).last()

        if song_entry and song_entry.jam_entry.jam_id != self.jam_entry.jam_id:
            raise errors.NotSingleJamUsedError

    def _assign_next_order_number(self):
        last_stream_entry = SongEntry.objects.filter(
            song=self.song,
            stream=self.stream
        ).exclude(pk=self.pk).order_by('order').last()

        if last_stream_entry:
            self.order = last_stream_entry.order + 1

    def save(self, **kwargs):
        self._validate_jam_is_the_same()
        self._assign_next_order_number()
        return super(SongEntry, self).save(**kwargs)
