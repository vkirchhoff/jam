from rest_framework import viewsets, mixins
from rest_framework.parsers import MultiPartParser
from .models import Instrument, Jam, JamEntry, Song, SongEntry
from .serializers import (
    InstrumentSerializer, JamListSerializer, JamEditSerializer,
    JamEntryListSerializer, JamEntryEditSerializer,
    SongListSerializer, SongCreateSerializer
)


class InstrumentViewSet(viewsets.ModelViewSet):
    def get_queryset(self):
        return Instrument.objects.all()

    def get_serializer_class(self):
        return InstrumentSerializer


class JamViewSet(viewsets.ModelViewSet):
    def get_queryset(self):
        return Jam.objects.prefetch_related('required_instruments', 'entries').all()

    def get_serializer_class(self):
        if self.action in ('list', 'retrieve',):
            return JamListSerializer
        return JamEditSerializer


class JamEntryViewSet(viewsets.ModelViewSet):
    parser_classes = [MultiPartParser]

    def get_queryset(self):
        return JamEntry.objects.all()

    def get_serializer_class(self):
        if self.action in ('list', 'retrieve', ):
            return JamEntryListSerializer
        return JamEntryEditSerializer


class SongViewSet(mixins.ListModelMixin,
                  mixins.RetrieveModelMixin,
                  mixins.CreateModelMixin,
                  viewsets.GenericViewSet):
    def get_queryset(self):
        return Song.objects.prefetch_related('entries', 'entries__jam_entry').all()

    def get_serializer_class(self):
        if self.action in ('list', 'retrieve', ):
            return SongListSerializer
        return SongCreateSerializer
