class NonSupportedInstrumentError(Exception):
    pass


class NotEnoughInstrumentsError(Exception):
    pass


class NotSingleJamUsedError(Exception):
    pass
