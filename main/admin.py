from django.contrib import admin
from .models import Instrument, Jam, JamEntry, Song, SongEntry


@admin.register(Instrument)
class InstrumentAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', ]
    list_display_links = ['id', 'name', ]
    readonly_fields = ['id', ]

    fieldsets = (
        (None, {
            'fields': ['id', 'name', 'description', ],
        }),
    )


class JamEntryInline(admin.TabularInline):
    model = JamEntry
    extra = 0
    fields = ['id', 'user', 'instrument', 'sound_file', ]
    readonly_fields = ['id', ]


@admin.register(Jam)
class JamAdmin(admin.ModelAdmin):
    list_display = ['id', 'status', 'date_started', 'date_finished', ]
    list_display_links = ['id', 'status', 'date_started', 'date_finished', ]
    readonly_fields = ['id', ]

    fieldsets = (
        (None, {
            'fields': ['id', 'status', 'date_started', 'date_finished', 'required_instruments', ],
        }),
    )

    inlines = [JamEntryInline, ]


class SongEntryInline(admin.TabularInline):
    model = SongEntry
    extra = 0
    fields = ['id', 'jam_entry', 'stream', 'order', 'lasts', ]
    readonly_fields = ['id', ]


@admin.register(Song)
class SongAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'sound_file']
    list_display_links = ['id', 'name', 'sound_file']
    readonly_fields = ['id', 'sound_file', ]

    fieldsets = (
        (None, {
            'fields': ['id', 'name', 'sound_file', 'description'],
        }),
    )

    inlines = [SongEntryInline, ]
