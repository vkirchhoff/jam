import factory
from factory.django import DjangoModelFactory
from django.contrib.auth import get_user_model
from main.models import Instrument, Jam, JamEntry

User = get_user_model()


class UserFactory(DjangoModelFactory):
    class Meta:
        model = User
        django_get_or_create = ('username', )

    username = factory.Sequence(lambda n: f"user{n}")

    @factory.post_generation
    def instruments(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for instrument in extracted:
                self.instruments.add(instrument)


class InstrumentFactory(DjangoModelFactory):
    class Meta:
        model = Instrument

    name = factory.Sequence(lambda n: f"Instrument #{n}")


class JamFactory(DjangoModelFactory):
    class Meta:
        model = Jam

    @factory.post_generation
    def required_instruments(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for required_instrument in extracted:
                self.required_instruments.add(required_instrument)


class JamEntryFactory(DjangoModelFactory):
    class Meta:
        model = JamEntry
