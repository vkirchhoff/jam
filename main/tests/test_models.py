from django.test import TestCase
from django.core.files import File
from . import factories as f
from main import errors


class JamTestCase(TestCase):
    def setUp(self):
        self.instruments = [f.InstrumentFactory() for i in range(3)]
        self.user = f.UserFactory.create(instruments=self.instruments[0:2])
        self.another_user = f.UserFactory.create(instruments=self.instruments[2:3])

    def test_jamentry_can_have_only_users_instrument(self):
        jam = f.JamFactory.create(required_instruments=self.instruments[1:3])
        jam_entry_1 = f.JamEntryFactory(jam=jam, user=self.user, instrument=self.instruments[1])
        assert jam_entry_1

        try:
            jam_entry_2 = f.JamEntryFactory(jam=jam, user=self.another_user, instrument=self.instruments[2])
        except errors.NonSupportedInstrumentError:
            assert True

    def test_jam_starts_if_has_all_required_instruments(self):
        jam = f.JamFactory.create(required_instruments=self.instruments[1:3])
        jam_entry_1 = f.JamEntryFactory(jam=jam, user=self.user, instrument=self.instruments[1])
        jam_entry_2 = f.JamEntryFactory(jam=jam, user=self.another_user, instrument=self.instruments[2])
        try:
            jam.start()
            assert True
        except errors.NotEnoughInstrumentsError:
            assert False

    def test_jam_doesnt_start_if_has_not_enough_instruments(self):
        jam = f.JamFactory.create(required_instruments=self.instruments[1:3])
        jam_entry_1 = f.JamEntryFactory(jam=jam, user=self.user, instrument=self.instruments[0])
        jam_entry_2 = f.JamEntryFactory(jam=jam, user=self.another_user, instrument=self.instruments[2])
        try:
            jam.start()
            assert False
        except errors.NotEnoughInstrumentsError:
            assert True

    def test_jam_entry_validates_file_extension_correctly(self):
        jam = f.JamFactory.create(required_instruments=self.instruments[1:3])
        with open('main/tests/samples/drums.wav', 'rb') as file:
            jam_entry_1 = f.JamEntryFactory(
                jam=jam,
                user=self.user,
                instrument=self.instruments[1],
                sound_file=File(file, name='drums.wav')
            )
            assert True
        try:
            with open('main/tests/samples/drums.txt', 'rb') as file:
                jam_entry_2 = f.JamEntryFactory(
                    jam=jam,
                    user=self.another_user,
                    instrument=self.instruments[2],
                    sound_file=File(file, name='drums.txt')
                )
            assert False
        except ValueError:
            assert True
