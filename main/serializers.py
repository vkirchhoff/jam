from rest_framework import serializers
from .models import Instrument, Jam, JamEntry, Song, SongEntry
from django.contrib.auth import get_user_model

User = get_user_model()


class InstrumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Instrument
        fields = ['id', 'name', 'description', ]


class JamEntryListSerializer(serializers.ModelSerializer):
    class Meta:
        model = JamEntry
        fields = ['id', 'user', 'instrument', 'sound_file', ]


class JamEntryEditSerializer(serializers.ModelSerializer):
    sound_file = serializers.FileField(required=False, allow_null=True)

    class Meta:
        model = JamEntry
        fields = ['id', 'jam', 'user', 'instrument', 'sound_file', ]


class JamListSerializer(serializers.ModelSerializer):
    required_instruments = InstrumentSerializer(many=True)
    entries = JamEntryListSerializer(many=True)

    class Meta:
        model = Jam
        fields = ['id', 'status', 'date_started', 'date_finished', 'required_instruments', 'entries', ]


class JamEditSerializer(serializers.ModelSerializer):
    date_started = serializers.DateTimeField(required=False, allow_null=True)
    required_instruments = serializers.PrimaryKeyRelatedField(
        many=True, required=False, allow_null=True,
        queryset=Instrument.objects.all()
    )

    class Meta:
        model = Jam
        fields = ['id', 'status', 'date_started', 'date_finished', 'required_instruments', ]


class SongEntryListSerializer(serializers.ModelSerializer):
    jam_entry = JamEntryListSerializer()

    class Meta:
        model = SongEntry
        fields = ['id', 'jam_entry', 'stream', 'order', 'lasts', ]


class SongEntryEditSerializer(serializers.ModelSerializer):
    jam_entry = serializers.PrimaryKeyRelatedField(
        queryset=JamEntry.objects.all()
    )

    class Meta:
        model = SongEntry
        fields = ['id', 'jam_entry', 'stream', 'order', 'lasts', ]


class SongListSerializer(serializers.ModelSerializer):
    entries = SongEntryListSerializer(many=True)

    class Meta:
        model = Song
        fields = ['id', 'name', 'sound_file', 'entries', 'description', ]


class SongCreateSerializer(serializers.ModelSerializer):
    entries = SongEntryEditSerializer(many=True)

    class Meta:
        model = Song
        fields = ['id', 'name', 'entries', 'description', ]

    def create(self, validated_data):
        entries = validated_data.pop('entries')
        instance = Song.objects.create(
            **validated_data
        )
        for entry in entries:
            SongEntry.objects.create(song_id=instance.id, **entry)
        instance.create()
        return instance
