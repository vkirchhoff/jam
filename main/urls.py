from django.urls import path, include

from rest_framework import routers
from user.viewsets import UserViewSet
from .viewsets import (
   InstrumentViewSet, JamViewSet, JamEntryViewSet,
   SongViewSet,

)

from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi


router = routers.DefaultRouter(trailing_slash=True)
router.register('user', UserViewSet, basename='user')
router.register('instrument', InstrumentViewSet, basename='instrument')
router.register('jam', JamViewSet, basename='jam')
router.register('jam-entry', JamEntryViewSet, basename='jam-entry')
router.register('song', SongViewSet, basename='song')


schema_view = get_schema_view(
   openapi.Info(
      title="Jam API",
      default_version='v1',
      description="Jam API description",
      contact=openapi.Contact(email="titanibiskf@gmail.com"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny, ),
)

urlpatterns = [
    path('', include(router.urls)),
    path('docs/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
]
